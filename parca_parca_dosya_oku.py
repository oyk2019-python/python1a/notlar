import os
from math import ceil

def doyayi_oku(file_name):
    data = []
    with open(file_name) as file:
        opened_data = ""
        for bytes in range(ceil(os.path.getsize(file_name)/1024)):

            opened_data += file.read(1024)
            if opened_data.find("\\") > -1:
                for i in opened_data.split("\\")[:-1]:
                    yield i
                opened_data = opened_data.split("\\")[-1]

for i in doyayi_oku('data.txt'):
    print(i)
