class PhoneBook:
    persons = []
    search_keyword = ""

    def add(self, name, surname, telephone):
        self.persons.append({
            "name": name,
            "surname": surname,
            "telephone": telephone
        })

    def delete(self, index):
        self.persons.pop(index)

    def list(self):
        item_index = 0
        for i in self.persons:
            item_index += 1
            print("{index} - {name}, {surname}, {telephone}".format(
                index=item_index,
                name=i.get('name'),
                surname=i.get('surname'),
                telephone=i.get('telephone'),
            ))

    def update(self, index, name, surname, telephone):
        self.persons[index].update({
            "name": name,
            "surname": surname,
            "telephone": telephone
        })

    def search_algorithm(self, elem):
        if elem.get('name').lower().startswith(self.search_keyword):
            return True
        return False

    def search(self, keyword):
        self.search_keyword = keyword
        for i in filter(self.search_algorithm, self.persons):
            print("{name}, {surname}, {telephone}".format(
                name=i.get('name'),
                surname=i.get('surname'),
                telephone=i.get('telephone'),
            ))

    def menu(self):
        print('Please select')
        print('1 - new entery')
        print('2 - list entery')
        print('3 - delete entery')
        print('4 - update entery')
        print('5 - search entery')

