import csv
import random
def calculate(data):
    if data.get('GPS Altitude(m)') != "-" and data.get('CO₂ in g/km (Instantaneous)(g/km)') != "-" and data.get('Speed (GPS)(km/h)') !="-":
        alt = float(data.get('GPS Altitude(m)'))
        km = float(data.get('Speed (GPS)(km/h)'))
        co2 = float(data.get('CO₂ in g/km (Instantaneous)(g/km)'))
        if co2 > 0 and km > 0 and alt > 0:
            new_data = {
                'co2':co2,
                'km':km,
                'alt':alt,
                'hesaplama':  co2 / alt
            }
            return new_data

with open('trackLog-2019-Jul-15_15-22-47.csv',
          newline='') as csvfile:
    spamreader = csv.DictReader(csvfile,
                            delimiter=',',
                            quotechar='"')
    data_frames = list(spamreader)
    oran = 100
    oran_data = None

    oran2 = -1
    oran_data2 = None

    for i in list(map(calculate,data_frames)):
        if i is not None:
            if oran2< i.get('hesaplama'):
                oran2 = i.get('hesaplama')
                oran_data2 = i
            if oran > i.get('hesaplama'):
                oran = i.get('hesaplama')
                oran_data = i
    print(oran_data)
    print(oran_data2)
