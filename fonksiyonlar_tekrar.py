
def toplama(x,y,z=0):
    return x+y+z

toplam = toplama(1,3)
print(toplam)

def toplama2(x,y,z=0):
     x+y+z

print(toplama2(1,2,3))

def toplama3(x,y,z=0,h=1):

    return x+y+z+h


print(toplama3(1,2,h=3))

def toplama4(x,y,z=0,h=1):
    k = x+y+z+h

k = toplama4(1,2)
print(k)

def toplama5(*args):
    print(sum(args))

toplama5(1,2,3,4,5,6,7,9,4,5,6,7,8,9)

def toplama6(**kwargs):
    print(kwargs)

toplama6(a=1,b=2,c=3,d=4)

def toplam7(*args,**kwargs):
    print(args)
    print(kwargs)
toplam7(1,2,3,4)
toplam7()
def toplam8():
    print(6+7+8)
toplam8()

def toplam9(x,y,*args):
    print(sum(args) + x + y)
    print(args)

print(toplam9(1,2,4))

def toplama10(x,y,z=1,*args):
    return args,x,y,z

print(toplama10(3,5,9,6))


def toplama11(x,y,z=1,**kwargs):
    return kwargs,x,y,z

print(toplama11(1,2,denene=2))

def toplama12(x,y,z=1,*args,**kwargs):
    return kwargs,args,x,y,z

print(toplama12(1,3,4,6,7,deneme=2,deli=1))
