import os
from random import randint

from games.Level import Level
from games.Oyuncu import Oyuncu


class Game:
    basladimi = False
    karakter = None
    level = None

    def __init__(self):
        print('Oyuna Başlamak için isim girin')
        karakter_ismi = input('isminiz :')
        self.karakter = Oyuncu()
        self.karakter.isim = karakter_ismi
        self.karakter.deneyim_seviyesi = 2
        self.level = Level(self.karakter)
        print('Hoş geldin {} '.format(self.karakter.isim))
        self.run()
    def hasar_hesap(self, kimden, kime):
        hasar = kimden.silahi.get('hasar') * kimden.deneyim_seviyesi
        hasardan_kacma = randint(0,10)
        if hasardan_kacma > 6:
            return 0
        kritik = randint(0,10)
        if kritik < 3:
            hasar = hasar * 3
        hasar = hasar - (hasar * (0.1 * kime.deneyim_seviyesi))
        return hasar

    def run(self):
        while True:
            os.system('clear')
            print('1 - Haraket et')
            print('2 - Etrafa Bak')
            print('3 - Çantaya Bak')
            print('4 - Kötüleri Ara')
            islem = input('Seçim : ')
            if islem == "1":
                os.system('clear')
                print('1 - Yukarı')
                print('2 - Aşağı')
                print('3 - Sağa')
                print('4 - Sola')
                yon = input('Hangi Yöne :')
                """
                yon = 0 yukarı
                yon = 1 sol
                yon = 2 sag
                yon = 3 asagi
                """
                if yon == '1':
                    self.level.ilerle(1, 0)

                if yon == '4':
                    self.level.ilerle(1, 1)

                if yon == '3':
                    self.level.ilerle(1, 2)

                if yon == '2':
                    self.level.ilerle(1, 3)
                print('ilerledin')
                print(self.level.ana_karakter_kare)
                input('Menüye dönmek için bir tuşa bas')
            if islem == "2":
                self.level.etrafa_bak()
                print('Birisi ile konuşmak istiyor musun ?')
                evet = input('E/H :')
                if evet.strip().lower() == 'e':
                    kim = input('Kimle ?')
                    self.level.etkilesim(int(kim))
                    print('0 - Çıkış')
                    secim = input('Hangisini Satın Alacaksın :')
                    if secim == '0':
                        continue
                    urun = self.level.etkilesim(int(kim),int(secim))
                    if self.karakter.altin >= urun.get('fiyat'):
                        self.karakter.altin -=urun.get('fiyat')
                        self.karakter.cantasi.append(urun)
                        print('kalan altın {}, alışveriş için teşekkürler'.format(
                            self.karakter.altin
                        ))
                    else:
                        print('Yeterli Paran yok')

                    print(self.karakter.cantasi)
            elif islem == "4":
                self.level.kotuleri_ara()
                print('0 - Vazgeç')
                kotu = input('Saldırmak İstediğini Seç :')
                if kotu == "0":
                    continue


                kotu_karakter = self.level.kotuler[int(kotu)-1]
                print(kotu_karakter)
                while True:

                    input('Saldır :')
                    os.system('clear')
                    hasarben = self.hasar_hesap(self.karakter, kotu_karakter.get('kim'))
                    kotu_karakter.get('kim').hasar(hasarben)
                    if kotu_karakter.get('kim').yasiyormu:
                        print('{} hasar vurdun {} can kaldı'.format(hasarben, kotu_karakter.get('kim').can))
                    else:
                        print('Adamı öldürdün, {} altın kazandın'.format(kotu_karakter.get('kim').altin))
                        self.karakter.altin += kotu_karakter.get('kim').altin
                        input('Devam etmek için bir tuşa basın')
                        break
                    hasarkotu = self.hasar_hesap(kotu_karakter.get('kim'),self.karakter)
                    self.karakter.hasar(hasarkotu)
                    if self.karakter.yasiyormu:
                        print('{} hasar yedin, canın {} kaldı'.format(hasarkotu, self.karakter.can))
                    else:
                        print('Öldün')
                        exit()

            elif islem == "3":
                item_id = 0
                print('Altın {}'.format(self.karakter.altin))
                print('0 - Çıkış')
                for i in self.karakter.cantasi:
                    item_id +=1
                    print('{} - {}'.format(
                        item_id, i.get('isim')
                    ))
                islem = input('Kullanmak İstediğin :')





