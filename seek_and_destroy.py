
def doyayi_oku(file_name, howmanybytes,seekposition):
    with open(file_name) as file:
        file.seek(seekposition)
        return file.read(howmanybytes)


print(doyayi_oku("data.txt",1024,5))
print(doyayi_oku("data.txt",1024,1000000))