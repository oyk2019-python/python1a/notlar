import csv


def listele():
    with open("data.csv", newline='') as data:
        read = csv.DictReader(data, delimiter=',', quotechar='"')
        for row in read:
            print("Bicim: {} Icerik: {} Fiyat: {}"
                  .format(row.get('bicim'),
                          row.get('icerik'),
                          row.get('fiyat')))


def yaz():
    bicim_input = input('Odeme bicimini giriniz: ')
    icerik_input = input('Alisveris icerigini giriniz: ')
    fiyat_input = input('Fiyat giriniz: ')
    with open("data.csv", "a") as data:
        writer = csv.writer(data,
                            delimiter=',',
                            quotechar='"')
        writer.writerow([bicim_input, icerik_input, fiyat_input])
def ara():
    secim=input("""
    1- Nakit
    2- Kredi
    """)
    with open("data.csv", newline='') as data:
        read = csv.DictReader(data, delimiter=',', quotechar='"')
        for row in read:
            if secim == '1':
                if row.get('bicim') == 'Nakit':
                    print("Bicim: {} Icerik: {} Fiyat: {}"
                         .format(row.get('bicim'),
                                  row.get('icerik'),
                                  row.get('fiyat')))
            if secim == '2':
                if row.get('bicim') == 'Kredi':
                    print("Bicim: {} Icerik: {} Fiyat: {}"
                          .format(row.get('bicim'),
                                  row.get('icerik'),
                                  row.get('fiyat')))


while True:
    print('Yapmak istediginiz islemi secin:')
    secim = input('''
    1- Listele
    2- Yeni Kayit
    3- Ara
    0- Cikis
    ''')
    if secim == '1':
        listele()
    if secim == '2':
        yaz()
    if secim == '3':
        ara()
    if secim == '0':
        break
