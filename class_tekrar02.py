class Insan:
    goz = ""
    sac = ""
    kaskutlesi = ""
    __kalp_ritmi = 60

    def __init__(self, goz_rengi, sac_rengi):
        self.goz = goz_rengi
        self.sac = sac_rengi

    def setGoz(self, renk):
        self.goz = renk

    def kalpRitmi(self, ritim):
        self.__kalp_ritmi = ritim

    def kalpRitmimNe(self):
        return self.__kalp_ritmi


alper = Insan(goz_rengi='kahverngi',sac_rengi='siyah')
print(alper.kalpRitmimNe())